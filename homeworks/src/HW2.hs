 module HW2
        ( Contact (..)
        , isKnown
        ) where
 
 data Contact = On
             | Off
             | Unknown

              | Off
              | Unknown
 isKnown :: Contact -> Bool
 isKnown = error "isKnown"

isKnown x = case x of 
  Unknown -> False
  _       -> True
data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа

eval :: Term -> Int
           | Add Term Term       -- сложение
           | Sub Term Term       -- вычитание
           | Const Int           -- константа
   deriving Show
 eval :: Term -> Int
 eval = error "eval"

-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3
simplify :: Term -> Term
eval (Const x) = x
eval x = case x of
  Mult x y -> eval x * eval y 
  Add  x y -> eval x + eval y
  Sub  x y -> eval x - eval y
 -- Раскрыть скобки
 -- Mult (Add (Const 1) (Const 2)) (Const 3) ->
 -- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
 -- (1+2)*3 -> 1*3+2*3
 simplify :: Term -> Term
 simplify = error "simplify"

simplify (Mult x y) = simplify2 (simplify2 (Mult x y))
simplify x = simplify2 x
simplify2 :: Term -> Term
simplify2 (Const x) = Const x
simplify2 (Mult x (Add y z)) = simplify2(Add (Mult (simplify2 x) (simplify2 y)) (Mult (simplify2 x) (simplify2 z)))    
simplify2 (Mult (Add y z) x) = simplify2(Add (Mult (simplify2 y) (simplify2 x)) (Mult (simplify2 z) (simplify2 x)))   
simplify2 (Mult x (Sub y z)) = simplify2(Sub (Mult (simplify2 x) (simplify2 y)) (Mult (simplify2 x) (simplify2 z)))
simplify2 (Mult (Sub y z) x) = simplify2(Sub (Mult (simplify2 y) (simplify2 x)) (Mult (simplify2 z) (simplify2 x)))
simplify2 (Add x y) = Add (simplify2 x) (simplify2 y)
simplify2 (Sub x y) = Sub (simplify2 x) (simplify2 y)
simplify2 (Mult x y) = Mult (simplify2 x) (simplify2 y)