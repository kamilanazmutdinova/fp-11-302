 module HW4

        ( Show' (..)

        , A(..)

        , C(..)

        , Set(..)

        , symmetricDifference

        , fromBool

        , fromInt

        ) where

 

 class Show' a where

   show' :: a -> String

 

 data A = A Int

        | B


instance Show' A where

    show' a = case a of

        A i -> "A"++concatNums [i]

        _ -> "B"

 

 data C = C Int Int



instance Show' C where

    show' (C a b) = "C"++concatNums [a,b]


concatNums :: [Int] -> String

concatNums arr = foldl (\a b -> a++" "++b) "" $ map getNumber arr

    where getNumber i

           | i < 0 = "("++show i++")"

           | otherwise = show i


 

 data Set a = Set (a -> Bool)

 


 symmetricDifference :: Set a -> Set a -> Set a

symmetricDifference = undefined

symmetricDifference (Set f1) (Set f2) = Set $ \elem -> (f1 elem || f2 elem) && not (f1 elem && f2 elem)

 

fromBool = undefined

fromBool bool

    | bool = \t f -> t

    | otherwise = \t f -> f

 


fromInt = undefined



fromInt n

    | n == 0 = \s z -> z

    | otherwise = \s z -> s $ fromInt (n - 1) s z