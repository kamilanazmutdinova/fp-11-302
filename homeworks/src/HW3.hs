 module HW3

        ( foldr'

        , groupBy'

        , Either' (..)

        , either'

        , lefts'

        , rights'

        ) where

 foldr' :: (a -> b -> b) -> b -> [a] -> b

foldr' f b as = error "foldr'"

foldr' _ b [] = b

foldr' func b (x:xs) = func x (foldr' func b xs)

 
groupBy' :: (a -> a -> Bool) -> [a] -> [[a]]

groupBy' cmp as = error "groupBy'"

groupBy' _ [] = []

groupBy' cmp (x:xs) = (x:ys) : groupBy' cmp zs 

			where (ys,zs) = span' (cmp x) xs



span' :: (a -> Bool) -> [a] -> ([a], [a])

span' _ [] = ([],[])

span' pred list@(x:xs)

		| pred x         = (x:ys, zs)

		| otherwise   = ([], list)

        	where (ys, zs) = span' pred xs

 

 data Either' a b = Left' a

                  | Right' b

                  deriving (Show)

 either' :: (a -> c) -> (b -> c) -> Either' a b -> c

-either' f g eab = error "either'"

+either' f _ (Left' a) = f a

+either' _ g (Right' b) = g b


 lefts' :: [Either' a b] -> [a]

lefts' eabs = error "lefts'"


lefts' [] = []

lefts' (x:xs) = case x of

	Left' a -> a : lefts' xs

	Right' _ -> lefts' xs

 rights' :: [Either' a b] -> [b]

rights' eabs = error "rights'"



rights' [] = []

rights' (x:xs) = case x of

	Right' a -> a : rights' xs

	Left' _ -> rights' xs