 module HW5

        ( Parser (..)

        , dyckLanguage

        , Arith (..)

        , arith

        , Optional (..)

        ) where

 

 type Err = String

 data Parser a = Parser

                 { parse :: String ->

                   Either Err (a, String) }

 

 instance Functor Parser where

   fmap f p = Parser $ \s -> case parse p s of

     Left err -> Left err

     Right (x, rest) -> Right (f x, rest)

 

 instance Applicative Parser where

   pure a = Parser $ \s -> Right (a,s)

   (<*>) pf pa = Parser $ \s -> case parse pf s of

     Left err -> Left err

     Right (f,r1) -> case parse pa r1 of

       Left err -> Left err

       Right (x,r2) -> Right (f x, r2)

 

 anyChar :: Parser Char

 anyChar = Parser $ \s -> case s of

   [] -> Left "No chars left"

   (c:cs) -> Right (c, cs)

 

 many :: Parser a -> Parser [a]

 many p = Parser $ \s -> case parse p s of

   Left _ -> Right ([],s)

   Right (x,r1) -> case parse (many p) r1 of

     Left _ -> Right ([x],r1)

     Right (xs,r2) -> Right (x:xs, r2)

 

 matching :: (Char -> Bool) -> Parser Char

 matching p = Parser $ \s -> case s of

   [] -> Left "No chars left"

   (c:cs) | p c -> Right (c, cs)

          | otherwise -> Left $ "Char " ++ [c] ++

                         " is wrong"

 

 digit :: Parser Char

 digit = matching (\c -> '0'<=c && c<='9')

 

 char :: Char -> Parser Char

 char c = matching (==c)

 

 number :: Parser Int

 number = pure read <*> many digit

            -- read <$> many digit

 

 (<|>) :: Parser a -> Parser a -> Parser a

 (<|>) pa pb = Parser $ \s -> case parse pa s of

   Left err -> parse pb s

   Right a -> Right a


 dyckLanguage :: Parser String

dyckLanguage = undefined

dyckLanguage = Parser $ \s -> if correct s 0 then Right (s, "") else Left "PARSE FAIL"

    where correct "" count = count == 0

          correct (brace:left) count = correct left (if brace == '(' then count + 1 else count -1)

 

 data Arith = Plus Arith Arith

            | Minus Arith Arith

            | Mul Arith Arith

            | Number Int

           | Oper Char

            deriving (Eq,Show)

 

isOperation :: Char -> Bool

isOperation symbol = symbol == '+' || symbol == '-' || symbol == '*' || symbol == '/'



priority :: Char -> Integer

priority operation = snd $ head $ filter (\(oper, prior) -> oper == operation) [('(', 0),(')', 1),('+', 2),('-',2),('*', 3),('/',3)]



data Token = Operation Char | Num Int



parseToRPN :: String -> String -> [Token] -> [Token]

parseToRPN "" [] output = output

parseToRPN "" (s:otherStack) output = parseToRPN "" otherStack (output++[Operation s])

parseToRPN str@(symbol:symbols) stack output

    | stack == [] && isOperation symbol || symbol == '(' = parseToRPN symbols (symbol : stack) output
    | isOperation symbol = getPriorityOperations symbols stack symbol output

    | symbol == ')' = getOperations symbols stack output

    | otherwise = checkEither (parse number str)

        where checkEither (Right (num, leftStr)) = parseToRPN leftStr stack (output++[Num num])



getOperations left stack output =

        case stack of

            [] -> parseToRPN left stack output

            oper : otherOpers

                | oper == '(' -> parseToRPN left otherOpers output

                | otherwise -> getOperations left otherOpers (output++[Operation oper])



getPriorityOperations leftStr stack symbol output =

        case stack of

            [] -> parseToRPN leftStr (symbol:stack) output

            oper : otherOpers

                | (priority oper) > (priority symbol) -> getPriorityOperations leftStr otherOpers symbol (output++[Operation oper])

                | otherwise -> parseToRPN leftStr (symbol:oper:otherOpers) output



finalParse :: [Arith] -> Int -> Either Err Arith

finalParse [Number n1] _ = Right (Number n1)

finalParse (a1:a2:(Oper o):[]) _ = Right $ snd $ head $ filter (\(oper, arith) -> oper == o)

                [('+',(Plus a1 a2)), ('-',(Minus a1 a2)), ('*',(Mul a1 a2))]

finalParse tokens i = case tokens!!i of

    Oper o

        | i < 2 || length tokens < 3 -> Left ""

        | o == '+' -> finalParse ((getBefore tokens (i - 2))++[Plus a1 a2]++(getAfter tokens i)) 0

        | o == '-' -> finalParse ((getBefore tokens (i - 2))++[Minus a1 a2]++(getAfter tokens i)) 0

        | o == '*' -> finalParse ((getBefore tokens (i - 2))++[Mul a1 a2]++(getAfter tokens i)) 0

                where (a1, a2) = (tokens!!(i - 2), tokens!!(i - 1))

                      getBefore tokens idx = take idx tokens

                      getAfter tokens idx = drop (idx + 1) tokens

    _ -> finalParse tokens (i + 1)

finalParse _ _ = Left ""



toArith :: [Token] -> [Arith]

toArith arr = map convertToken arr

    where convertToken (Operation o) = Oper o

          convertToken (Num n) = Number n



 arith :: Parser Arith

arith = undefined

arith = Parser $ \s -> case finalParse (toArith (parseToRPN s [] [])) 0 of

    Left _ -> Left "Can't parse the expression."

    right@(Right arith) -> Right (arith, "")


 

 data Optional a = NoParam

                 | Param a

                 deriving (Eq,Show)

 

 instance Functor Optional where

  fmap = undefined

    fmap f NoParam = NoParam

    fmap f (Param val) = Param (f val)

 

 instance Applicative Optional where

  pure = undefined

  (<*>) = undefined

    pure = Param

    NoParam <*> _ = NoParam

    _ <*> NoParam = NoParam

    (Param f) <*> (Param val) = Param (f val)

 