# СЕМЕСТРОВАЯ 1

Лямбда-исчисление - модель ФП

## Термы

```
t ::= x               (переменная из множества X)
    | \lambda x . t   (функция принимает x и возвращает t)
    | t1 t2           (в t1 передаётся аргумент - t2)
```

Например,

```
\x . \y . x y
\x . \y . \z . x (x y z)  = \x . (\y . (\z . x ((x y) z)))
```

## Правила вычисления

```
(\x . t12) t2  ->  t12[x=t2]
```

```
(\x . \y . x y) (\z . z) (\u . \v . v)  ->
(\y . (\z . z) y) (\u . \v . v)         ->
(\z . z) (\u . \v . v)                  ->
\u . \v . v
```

NB! При замене нужно правильно работать с именами переменных

```
(\x . \y . x y) (\x . x) (\x . \y . y)
```

Есть варианты правил вычисления. У нас:
заменять только вне лямбд и слева направо

Пример. (\x . (\x . x) x) никуда не вычисляется

# Задание

Написать типы и функцию вычисления терма на один шаг и на много шагов

```
data Term = ... -- ???

eval1 :: Term -> Maybe Term
eval1 = ???

eval :: Term -> Term
eval = ???

-- другой вариант

canBeEvaluated :: Term -> Bool
canBeEvaluated = ???

eval1' :: Term -> Term
eval1' = ???
```


Желательны тесты (рукописные хотя бы)

## Сроки

4 марта - 4 апреля





module Sem1 where
import Data.Char

data Term1 = Var Char
        | App Term1 Term1
        | Lam Term1 Term1
        deriving (Eq)

instance Show Term1 where
    show (Var v) = [v]
    show (Lam v t) = "(\\" ++ show v ++ " . " ++ show t ++ ")"
    show (App t1 t2) = show t1 ++ " " ++ show t2


eval1 :: Term1 -> Term1
eval1 (App a1@(App t1 t2) t3) = App (eval1 a1) t3
eval1 (App t1 a1@(App t2 t3)) = App t1 (eval1 a1)
eval1 (App (Lam (Var var) t1) t2) = reduce var t2 t1
eval1 term = term 


reduce :: Char -> Term1 -> Term1 -> Term1
reduce var t (Var v) = if v == var then t else Var v
reduce var t lam@(Lam (Var v) (Var v1))
        | v /= v1 && v1 == var = Lam (Var v) (substituteVariable t lam)
        | otherwise = lam
reduce var t lam@(Lam (Var v) (Lam v2 t1))
        | v == var = lam
        | otherwise = Lam (Var v) (reduce var (substituteVariable t lam) (Lam v2 t1))
reduce var t lam@(Lam (Var v) (App t1 t2))
        | v == var = lam
        | otherwise = Lam (Var v) (reduce var (substituteVariable t lam) (App t1 t2))
reduce var t app@(App t1 t2) = App (reduce var (substituteVariable t app) t1)
                                   (reduce var (substituteVariable t app) t2)

substituteVariable t term = case t of
      Var v -> Var (substituteOnFreeVar (getUsedVars term) 'a')
      t -> t
      where substituteOnFreeVar usedVars var
                    | elem var usedVars = substituteOnFreeVar usedVars (chr (ord var + 1))
                    | otherwise = var

getUsedVars (Lam (Var v) t) = v : getUsedVars t
getUsedVars (App t1 t2) = getUsedVars t1 ++ getUsedVars t2
getUsedVars (Var v) = [v]


eval_many :: Term1 -> Term1
eval_many t@(App a1@(App t1 t2) t3) = eval_many (App (eval_many a1) t3)
eval_many t@(App t1 a1@(App t2 t3)) = eval_many (App t1 (eval_many a1))
eval_many t@(App (Lam (Var v1) t1) t2) = eval_many (reduce v1 t2 t1)
eval_many t = t
